## Tutorials

## MPLS

### Label encoding
```
Label: Label Value, 20 bits
Exp: Experimental Use, 3 bits
S: Bottom of Stack, 1 bit
TTL: Time to Live, 8 bits
```

### Parser
```
state parse_ethernet {
  packet.extract(hdr.ethernet);
  transition select(hdr.ethernet.etherType) {
    TYPE_MPLS: parse_mpls;
    TYPE_IPV4: parse_ipv4;
    default: accept;
  }
}

state parse_mpls {
  packet.extract(hdr.mpls.next);
  transition select(hdr.mpls.last.s) {
    1: parse_ipv4;
    default: parse_mpls;
  }
}

state parse_ipv4 {
  packet.extract(hdr.ipv4);
  transition accept;
}
```

### Ingress Pipeline Control Login
```
if(hdr.ipv4.isValid()){
    FEC_tbl.apply();
}
if(hdr.mpls[0].isValid()){
    mpls_tbl.apply();
}
```


## RSVP

Match `FEC_tbl` (actions are `ipv4_forward` or `mpls_ingress_n_hop`) on both, `source` and `destination` since we want to establish paths pairwise.

Good illustration of how meters work.The controller defines the meter rates.
Direct meter associated to `FEC_tbl` and read by the ingress switch. If value read above a threshold, drop.

- if B > Tp the packet is marked red.
- if B < Tp and B > Tc the packet is marked yellow.
- otherwise, the packet is marked green.
<p style="text-align: center;"> <img src="https://gitlab.ethz.ch/varxth/adv-net-solutions/-/raw/theo/03-RSVP/images/two-color-meters.png" alt=" End-to-end LSP protection"> </p>

## Load balancing

### ECMP

#### Data plane

TABLE1: maps dest IP to call `set_n_hop` or `ecmp_group` actions.
- `ecmp_group` computes the ecmp number: Receive `num_hops` and `group_id`. Compute hash over flow `(src ip , dst ip, src port, dst port, protocol)`. Store hash in `metadata`.
- `set_n_hop` is the basic action that receives outgoing port and mac. Used when no ecmp.

TABLE2 applied if `ecmp_group` was applied:
- match on the group id and the ecmp hash number stored in metadata
- calls `set_n_hop` that receives outgoing port and mac

```
if(dst == neighbor):
  ip_forward(packet)
else:
  h = hash(flow)
  g = group(flow)

  add_mpls_lable(h, g)

  mpls_forward(packet)
```

#### Control plane
```python
for all (src, dst) in switches:
  paths = compute_equal_cost_shortest_paths(src, dst)

  # install table entries

  # Connect hosts to switches
  if(src == dst):
    for host in hosts(src):
      install_forwarding_rule(src, host)

  # Direct forward via MPLS
  else if(len(paths) == 1 and):
    for host in hosts(dst):
      install_next_hop_rule(src, host)

  # Forward via ECMP
  else:
    for host in host(dst):
      add_ecmp_group(host)
```

### Flowlet
Enhancement of ECMP: Re-hash after 50ms have passed between two packets.

#### Data plane
- 2 Registers:
  - `flowlet_to_id`: keeps random id of flowlet, this is added to the hash function to compute the ecmp hash number.
  Same `flowlet_to_id` --> same hash --> same forwarding
  - `flowlet_time_stamp`: Keep time stamp for the last observed packet of a
  flow.
- Registers are accessible by hash(flow)
- If `in_time - flowlet_time_stamp > Δ`: update `flowlet_to_id` by calling the random extern. The packet
  will take another path then one of the same flow before.

First, read registers and potentially update the flowlet id if inter-packet gap too large.
Then, apply the ipv4 table that matches on dest IP and calls `ecmp_group` or `set_n_hop` actions. If `ecmp_group` called, mac and port determined based on matched ecmp number.

#### Control plane

Same as in ECMP

## Traffic control

### Common Traffic Control Solutions

| Scenario                                             | Solution                                          |
| ---                                                  | ---                                               |
| Limit total bandwidth to a known rate                | TBF, HTB with child classes                       |
| Limit bandwidth of a particular service              | HTB classes and classifying with a filter traffic |
| Reserve bandwidth for application                    | HTB with children classes and classifying         |
| Prefer latency sensitive traffic                     | PRIO inside an HTB class                          |
| Allow equitable distribution of unreserved bandwidth | HTB with borrowing                                |
| Ensure that a particular type of traffic is dropped  | Policer attached to a filter with a drop action   |

- TBF: Token Bucket Filter
- HTB: Hierarchical Token Bucket


## BGP Free Core & BGP VPN over MPLS

Challenges:
- Reachability: Access `10.0.0.0/8` within company
- Isolation: Make `10.0.0.0/8` unaccessible for other companies
- Scalability: Do not overload ISP and use MPLS

### Steps
1. Configure BGP and a BGP Free Core
    1. Configure the iBGP sessions
    1. Configure the eBGP sessions
    1. Configure MPLS forwarding with LDP
1. Configure the VPN over BGP using VRF
    1. Configure eBGP in a VRF
    1. Configure the VPNs

## Fast reroute

### Control-plane

- Write 2 registers for the data plane: PrimaryNextHop and SecondaryNextHop, that for a destination index give the exit port. Also installs a table that maps destination hosts to an index (nexthop id) in each switch
- Controller rewrites the registers after failure notification. So the Primary and Secondary NH change over time. Secondary useful before the controller is notified.
- Controller’s computation of LFA: For a (switch, dst_host, next_hop) triplet, try all other neighbors to see if d(neighbor, dst_host) < d(neighbor, switch) + d(switch, dst_host)



### Data-plane

- Gets Nexthop id from table. Allows to read port from Primary and SecondaryNH registers.
- Another register (linkFailure) indicates for each link whether it’s detected down or up. Switch uses Secondary if link to Primary is down.

## IP multicast

### PIM

PIM is used between routers to establish multicast routing trees.

1. Activate PIM on all routers
2. Configure Rendez-vous point on all routers

Let the magic happen! See
[this video](https://www.youtube.com/watch?v=PhzMcUcS6UA) for an explanation.

### IGMP

1. Activate on all host-facing interfaces
2. Join a group via command line interface
